import { Button, Card, CardActionArea, CardActions, CardContent, CardMedia, Grid, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";

const VanzariGarsoniere: React.FunctionComponent<{}> = () => {
    const { t } = useTranslation();
    return <div>
        <h1 style={{ textAlign: "center" }}>{t('studio-apartments')}</h1>
        <div style={{ padding: "40px 0px 40px 140px" }}>

            <Grid
                container
                direction="row"
                rowSpacing={10}
                columnSpacing={10}
                alignItems="center"
                columns={4}
            >
                <Grid item>
                    <Card sx={{ width: 345 }} >
                        <CardActionArea>
                            <CardMedia
                                component="img"
                                height="345"
                                image="/images/gar4.jpg"
                                alt="green iguana"
                            />
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="div">
                                    {t('studio')} 1
                                </Typography>
                                <Typography variant="body2" color="text.secondary">
                                    <ul>
                                        <li>
                                            20mp
                                        </li>
                                        <li>
                                            Cluj, Marasti
                                        </li>
                                        <li>
                                            40.000 euro
                                        </li>
                                    </ul>
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                        <CardActions>
                            <Button size="small" color="primary">
                                {t('view')}
                            </Button>
                        </CardActions>
                    </Card>
                </Grid>
                <Grid item>
                    <Card sx={{ width: 345 }} >
                        <CardActionArea>
                            <CardMedia
                                component="img"
                                height="345"
                                image="/images/gar5.jpg"
                                alt="green iguana"
                            />
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="div">
                                    {t('studio')} 2
                                </Typography>
                                <Typography variant="body2" color="text.secondary">
                                    <ul>
                                        <li>
                                            25mp
                                        </li>
                                        <li>
                                            Cluj, {t('center')}
                                        </li>
                                        <li>
                                            30.000 euro
                                        </li>
                                    </ul>
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                        <CardActions>
                            <Button size="small" color="primary">
                                {t('view')}
                            </Button>
                        </CardActions>
                    </Card>
                </Grid>
            </Grid>
        </div>


    </div>
}


export default VanzariGarsoniere;