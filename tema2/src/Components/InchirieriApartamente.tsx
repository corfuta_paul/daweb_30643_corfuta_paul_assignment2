import { Button, Card, CardActionArea, CardActions, CardContent, CardMedia, Grid, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";


const InchirieriApartamente: React.FunctionComponent<{}> = () => {
    const { t } = useTranslation();
    return <div>
        <h1 style={{ textAlign: "center" }}>{t('apartments')}</h1>
        <div style={{ padding: "40px 0px 40px 140px" }}>

            <Grid
                container
                direction="row"
                rowSpacing={10}
                columnSpacing={10}
                alignItems="center"
                columns={4}
            >
                <Grid item>
                    <Card sx={{ width: 345 }} >
                        <CardActionArea>
                            <CardMedia
                                component="img"
                                height="345"
                                image="/images/ap1.jpg"
                                alt="green iguana"
                            />
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="div">
                                    {t('apartment')} 1
                                </Typography>
                                <Typography variant="body2" color="text.secondary">
                                    <ul>
                                        <li>
                                            50mp
                                        </li>
                                        <li>
                                            Cluj, Marasti
                                        </li>
                                        <li>
                                            350 euro {t('rent1')}
                                        </li>
                                        <li>
                                            3 {t('rooms')}
                                        </li>
                                    </ul>
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                        <CardActions>
                            <Button size="small" color="primary">
                                {t('view')}
                            </Button>
                        </CardActions>
                    </Card>
                </Grid>
                <Grid item>
                    <Card sx={{ width: 345 }} >
                        <CardActionArea>
                            <CardMedia
                                component="img"
                                height="345"
                                image="/images/ap2.jpg"
                                alt="green iguana"
                            />
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="div">
                                    {t('apartment')} 2
                                </Typography>
                                <Typography variant="body2" color="text.secondary">
                                    <ul>
                                        <li>
                                            60mp
                                        </li>
                                        <li>
                                            Cluj, {t('center')}
                                        </li>
                                        <li>
                                            400 euro {t('rent1')}
                                        </li>
                                        <li>
                                            3 {t('rooms')}
                                        </li>
                                    </ul>
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                        <CardActions>
                            <Button size="small" color="primary">
                                {t('view')}
                            </Button>
                        </CardActions>
                    </Card>
                </Grid>
                <Grid item >
                    <Card sx={{ width: 345 }} >
                        <CardActionArea>
                            <CardMedia
                                component="img"
                                height="345"
                                image="/images/ap3.jpg"
                                alt="green iguana"
                            />
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="div">
                                    {t('apartment')} 3
                                </Typography>
                                <Typography variant="body2" color="text.secondary">
                                    <ul>
                                        <li>
                                            70mp
                                        </li>
                                        <li>
                                            Cluj
                                        </li>
                                        <li>
                                            399 euro {t('rent1')}
                                        </li>
                                        <li>
                                            2 {t('rooms')}
                                        </li>
                                    </ul>
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                        <CardActions>
                            <Button size="small" color="primary">
                                {t('view')}
                            </Button>
                        </CardActions>
                    </Card>
                </Grid>
                <Grid item >
                    <Card sx={{ width: 345 }} >
                        <CardActionArea>
                            <CardMedia
                                component="img"
                                height="345"
                                image="/images/ap4.jpg"
                                alt="green iguana"
                            />
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="div">
                                    {t('apartment')} 4
                                </Typography>
                                <Typography variant="body2" color="text.secondary">
                                    <ul>
                                        <li>
                                            80mp
                                        </li>
                                        <li>
                                            Cluj
                                        </li>
                                        <li>
                                            500 euro {t('rent1')}
                                        </li>
                                        <li>
                                            4 {t('rooms')}
                                        </li>
                                    </ul>
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                        <CardActions>
                            <Button size="small" color="primary">
                                {t('view')}
                            </Button>
                        </CardActions>
                    </Card>
                </Grid>
                <Grid item >
                    <Card sx={{ width: 345 }} >
                        <CardActionArea>
                            <CardMedia
                                component="img"
                                height="345"
                                image="/images/ap5.jpg"
                                alt="green iguana"
                            />
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="div">
                                    {t('apartment')} 5
                                </Typography>
                                <Typography variant="body2" color="text.secondary">
                                    <ul>
                                        <li>
                                            90mp
                                        </li>
                                        <li>
                                            Cluj
                                        </li>
                                        <li>
                                            500 euro {t('rent1')}
                                        </li>
                                        <li>
                                            4 {t('rooms')}
                                        </li>
                                    </ul>
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                        <CardActions>
                            <Button size="small" color="primary">
                                {t('view')}
                            </Button>
                        </CardActions>
                    </Card>
                </Grid>

            </Grid>
        </div>


    </div>
}


export default InchirieriApartamente;