import { Button, Card, CardActionArea, CardActions, CardContent, CardMedia, Grid, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";

const VanzariApartamente: React.FunctionComponent<{}> = () => {
    const { t } = useTranslation();
    return <div>
        <h1 style={{ textAlign: "center" }}>{t('apartments')}</h1>
        <div style={{ padding: "40px 0px 40px 140px" }}>

            <Grid
                container
                direction="row"
                rowSpacing={10}
                columnSpacing={10}
                alignItems="center"
                columns={4}
            >
                <Grid item>
                    <Card sx={{ width: 345 }} >
                        <CardActionArea>
                            <CardMedia
                                component="img"
                                height="345"
                                image="/images/ap6.jpg"
                                alt="green iguana"
                            />
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="div">
                                    {t('apartment')} 1
                                </Typography>
                                <Typography variant="body2" color="text.secondary">
                                    <ul>
                                        <li>
                                            50mp
                                        </li>
                                        <li>
                                            Cluj, Marasti
                                        </li>
                                        <li>
                                            70.000 euro
                                        </li>
                                        <li>
                                            3 {t('rooms')}
                                        </li>
                                    </ul>
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                        <CardActions>
                            <Button size="small" color="primary">
                                {t('view')}
                            </Button>
                        </CardActions>
                    </Card>
                </Grid>
                <Grid item>
                    <Card sx={{ width: 345 }} >
                        <CardActionArea>
                            <CardMedia
                                component="img"
                                height="345"
                                image="/images/ap7.jpg"
                                alt="green iguana"
                            />
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="div">
                                    {t('apartment')} 2
                                </Typography>
                                <Typography variant="body2" color="text.secondary">
                                    <ul>
                                        <li>
                                            60mp
                                        </li>
                                        <li>
                                            Cluj, {t('center')}
                                        </li>
                                        <li>
                                            60.000 euro
                                        </li>
                                        <li>
                                            3 {t('rooms')}
                                        </li>
                                    </ul>
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                        <CardActions>
                            <Button size="small" color="primary">
                                {t('view')}
                            </Button>
                        </CardActions>
                    </Card>
                </Grid>
                <Grid item >
                    <Card sx={{ width: 345 }} >
                        <CardActionArea>
                            <CardMedia
                                component="img"
                                height="345"
                                image="/images/ap8.jpg"
                                alt="green iguana"
                            />
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="div">
                                    {t('apartment')} 3
                                </Typography>
                                <Typography variant="body2" color="text.secondary">
                                    <ul>
                                        <li>
                                            70mp
                                        </li>
                                        <li>
                                            Cluj
                                        </li>
                                        <li>
                                            50.000 euro
                                        </li>
                                        <li>
                                            2 {t('rooms')}
                                        </li>
                                    </ul>
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                        <CardActions>
                            <Button size="small" color="primary">
                                {t('view')}
                            </Button>
                        </CardActions>
                    </Card>
                </Grid>
                <Grid item >
                    <Card sx={{ width: 345 }} >
                        <CardActionArea>
                            <CardMedia
                                component="img"
                                height="345"
                                image="/images/ap9.jpg"
                                alt="green iguana"
                            />
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="div">
                                    {t('apartment')} 4
                                </Typography>
                                <Typography variant="body2" color="text.secondary">
                                    <ul>
                                        <li>
                                            80mp
                                        </li>
                                        <li>
                                            Cluj
                                        </li>
                                        <li>
                                            90.000 euro
                                        </li>
                                        <li>
                                            4 {t('rooms')}
                                        </li>
                                    </ul>
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                        <CardActions>
                            <Button size="small" color="primary">
                                {t('view')}
                            </Button>
                        </CardActions>
                    </Card>
                </Grid>
                <Grid item >
                    <Card sx={{ width: 345 }} >
                        <CardActionArea>
                            <CardMedia
                                component="img"
                                height="345"
                                image="/images/ap10.jpg"
                                alt="green iguana"
                            />
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="div">
                                    {t('apartment')} 5
                                </Typography>
                                <Typography variant="body2" color="text.secondary">
                                    <ul>
                                        <li>
                                            90mp
                                        </li>
                                        <li>
                                            Cluj
                                        </li>
                                        <li>
                                            100.000 euro
                                        </li>
                                        <li>
                                            4 {t('rooms')}
                                        </li>
                                    </ul>
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                        <CardActions>
                            <Button size="small" color="primary">
                                {t('view')}
                            </Button>
                        </CardActions>
                    </Card>
                </Grid>

            </Grid>
        </div>


    </div>
}


export default VanzariApartamente;