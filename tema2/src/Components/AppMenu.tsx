import { AppBar, Button, FormControl, IconButton, InputLabel, Select, SelectChangeEvent, Toolbar } from "@mui/material";
import { useNavigate } from "react-router-dom";
import './style.css';
import { MenuItem, Menu } from "@mui/material";
import { useEffect, useState } from "react";
import ReactCountryFlag from "react-country-flag"
import i18next from "i18next";
import { useTranslation } from "react-i18next";
import { useCookies } from "react-cookie";

const AppMenu: React.FunctionComponent<{}> = () => {
    const navigate = useNavigate();
    const [language, setLanguage] = useState<string>("");
    const { t } = useTranslation();

    const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
    const [anchorEl2, setAnchorEl2] = useState<null | HTMLElement>(null);
    const open = Boolean(anchorEl);
    const open2 = Boolean(anchorEl2);
    const [cookies, setCookie, removeCookie] = useCookies(['language']);

    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClick2 = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl2(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };
    const handleClose2 = () => {
        setAnchorEl2(null);
    };

    const handleInchirieriGarsoniere = () => {
        navigate("/inchirieri/garsoniere");
        handleClose();
    }

    const handleInchirieriApartamente = () => {
        navigate("/inchirieri/apartamente");
        handleClose();
    }

    const handleInchirieriCase = () => {
        navigate("/inchirieri/case");
        handleClose();
    }

    const handleVanzariGarsoniere = () => {
        navigate("/vanzari/garsoniere");
        handleClose2();
    }

    const handleVanzariApartamente = () => {
        navigate("/vanzari/apartamente");
        handleClose2();
    }

    const handleVanzariCase = () => {
        navigate("/vanzari/case");
        handleClose2();
    }

    useEffect(()=>{
        i18next.changeLanguage(cookies.language);
    },[])

    useEffect(()=>{
        if (language=="1"){
            setCookie("language", "en", {path: "/"});
        }
        if(language=="2"){
            setCookie("language", "ro", {path: "/"});
        }
    },[language])

    useEffect(()=>{
        if (cookies.language=="en"){
           i18next.changeLanguage("en");
        }
        if(cookies.language=="ro"){
            i18next.changeLanguage("ro");
        }
    },[cookies])

    const onChangeLanguage = (event: SelectChangeEvent) => {
        setLanguage(event.target.value as string);
    }

    return <AppBar position="static" style={{ backgroundColor: "#FFE4C4" }}>
        <Toolbar>
            <div>
            <Button className={"menu"} onClick={() => { navigate("/home") }}>{t('home')}</Button>
            <Button className={"menu"} onClick={() => { navigate("/noutati") }}>{t('news')}</Button>
            <Button className={"menu"} onClick={() => { navigate("/despre-noi") }}>{t('about-us')}</Button>
            <Button className={"menu"}
                aria-haspopup="true"
                onClick={handleClick}
            >
                {t('rent')}
            </Button>
            <Menu className={"menu"}
                id="basic-menu"
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                MenuListProps={{
                    'aria-labelledby': 'basic-button',
                }}
            >
                <MenuItem onClick={handleInchirieriGarsoniere}>{t('studio-apartments')}</MenuItem>
                <MenuItem onClick={handleInchirieriApartamente}>{t('apartments')}</MenuItem>
                <MenuItem onClick={handleInchirieriCase}>{t('houses')}</MenuItem>
            </Menu>
            <Button className={"menu"}
                aria-haspopup="true"
                onClick={handleClick2}
            >
                {t('sales')}
            </Button>
            <Menu className={"menu"}
                id="basic-menu"
                anchorEl={anchorEl2}
                open={open2}
                onClose={handleClose2}
                MenuListProps={{
                    'aria-labelledby': 'basic-button',
                }}
            >
                <MenuItem onClick={handleVanzariGarsoniere}>{t('studio-apartments')}</MenuItem>
                <MenuItem onClick={handleVanzariApartamente}>{t('apartments')}</MenuItem>
                <MenuItem onClick={handleVanzariCase}>{t('houses')}</MenuItem>
            </Menu>
            <Button className={"menu"} onClick={() => { navigate("/contact") }}>{t('contact')}</Button>
            </div>
            <FormControl className="menu">
                <InputLabel>{t('language')}</InputLabel>
                <Select

                    label={t('language')}
                    value={language}
                    onChange={onChangeLanguage}
                >
                    <MenuItem value={1}> <ReactCountryFlag countryCode="GB" svg style={{paddingRight : "10px"}}/>English</MenuItem>
                    <MenuItem value={2}> <ReactCountryFlag countryCode="RO" svg style={{paddingRight : "10px"}}/>Romanian</MenuItem>
                </Select>
            </FormControl>
        </Toolbar>
    </AppBar>

}

export default AppMenu;